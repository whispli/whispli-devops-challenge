const express = require('express');
const mysql = require('mysql');

const port = process.env.PORT || 3000;
const host = process.env.DB_HOST || 'localhost';
const user = process.env.DB_USER || 'root';
const password = process.env.DB_PASSWORD || '';
const database = process.env.DB_DATABASE || 'db';

const app = express();

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

var pool      =    mysql.createPool({
    connectionLimit : 100,
    host     : host,
    user     : user,
    password : password,
    database : database,
    debug    : true
});

app.get('/', (req, res) => {

    pool.query('SELECT 1', function (error) {

        let result = (error) ?
            {message: 'There was a problem connecting to the database.', error} :
            {message: 'Database connection successful.'};

        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(result));
    });
});

app.listen(port, () => console.log(`The api is listening on port ${port}...`));

# WEB

`/web` contains the simplest possible frontend, a single html page. 

The page contains a form that allows you to enter the url of your api. Submitting the form 
will submit a GET request to the specified api endpoint and render the result. This should 
be enough to allow you to test the frontend/backend connection.

If everything works, you'll see the success payload from your api rendered in a green box. 

![Web success screen](./success.png)

# API

`/api` contains a simple express server, packaged as a docker container. It connects 
to a mysql database. The database connection parameters are supplied as environment
variables. 

## Building

The simplest way to get started is to pull the docker image from dockerhub:

```
docker pull whispli/devops-challenge-api
```  

Of course, if you prefer, you can build your own image using the supplied [Dockerfile](./api/Dockerfile).

## Running

You can run the container using the following command. 

```bash
docker run \
-e DB_HOST='host.docker.internal' \
-e DB_USER='root' \
-e DB_PASSWORD='' \
-e DB_DATABASE='db' \
-p 3000:3000 \
-t whispli/devops-challenge-api
```

The parameters are as follows:

| | |
|---:|---|
| **DB_HOST**| The database hostname.
| **DB_USER**| The username used to connect to the database.
| **DB_PASSWORD**| The password used to connect to the database.     
| **DB_DATABASE**| The name of the database to connect to.     
    
## Testing

The api has one endpoint.

```
curl http://localhost:3000/
```

If the database connection is successful, the following json payload will be returned:

```javascript
{ message: "Database connection successful." }
```

If the database connection is unsuccessfull, the following json payload will be returned:

```javascript
{ message: 'There was a problem connecting to the database.', error }
```


